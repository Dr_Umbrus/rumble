﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public GameObject part2;
    public bool dieOnTime=false;  
    public float time=0;
    public float maxTime=0;
    public float lifeTime = 0;
    public float uses = 0;

    public bool permanent = false;

    public bool activated = false;

    SpriteRenderer sr;

    public int creator=-1;
    bool lifesteal = false;


    public void SetTrap(int cre, bool life)
    {
        creator = cre;
        lifesteal = life;
    }
    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!activated)
        {
            bool test = false;
            if(collision.tag == "player")
            {
                if (collision.GetComponent<Character>().playerNum != creator)
                {
                    test = true;
                }
            }
            else if(collision.tag == "damageable")
            {
                if (collision.GetComponent<Damageable>().id != creator)
                {
                    test = true;
                }
            }

            if(test)
            {
                GameObject temp = Instantiate(part2, transform.position, Quaternion.identity);
                temp.GetComponent<Projectile>().Create(creator, lifesteal);
                time = maxTime;
                activated = true;

                if (!permanent)
                {
                    uses--;
                    if (uses <= 0)
                    {
                        Destroy(gameObject);
                    }
                }
            }
            
        }
    }

    public void Update()
    {


        if (dieOnTime)
        {
            lifeTime -= Time.deltaTime;
        }

        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }

        if(activated)
        {
            time -= Time.deltaTime;
        }

        if(time<=0)
        {
            activated = false;
        }
    }

}
