﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worm : MonoBehaviour
{
    [SerializeField]
    float speed=0;
    [SerializeField]
    float lifetime = 0;


    private void Awake()
    {
        Destroy(gameObject, lifetime);
    }

    private void Update()
    {
        transform.Translate(Vector3.right * speed*Time.deltaTime, Space.Self);    
    }
}
