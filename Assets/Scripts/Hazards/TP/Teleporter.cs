﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Teleporter partner = null;
    public List<GameObject> traverse= new List<GameObject>();
    public Animator anim = null;
    [SerializeField]
    GameObject canvas = null;

    public ParticleSystem inside1 = null, inside2 = null, outside1 = null, outside2 = null;

    List<GameObject> top = new List<GameObject>();

    private void Awake()
    {
        anim = GetComponent<Animator>();
        inside1.Stop();
        outside1.Stop();
        inside2.Stop();
        outside2.Stop();
    }

    public void ShowCan(GameObject who)
    {
        canvas.SetActive(true);
        top.Add(who);
    }

    public void HideCan(GameObject who)
    {
        top.Remove(who);
        if (top.Count == 0)
        {
            canvas.SetActive(false);
        }
    }


    public void TPMe(GameObject col)
    {
        bool test = false;

        if (traverse.Count > 0)
        {

            for (int i = 0; i < traverse.Count; i++)
            {
                if (traverse[i] == col)
                {
                    test = true;
                }
            }
        }
        if (!test)
        {
            /*inside1.Play();
            partner.outside1.Play();
            inside2.Play();
            partner.outside2.Play();*/
            partner.traverse.Add(col);
            traverse.Add(col);
            StartCoroutine(WorkAgain(col));
            col.transform.position = new Vector3(partner.transform.position.x, partner.transform.position.y, col.transform.position.z);
            anim.SetTrigger("tp");
            partner.anim.SetTrigger("tp");
        }
    }

    IEnumerator WorkAgain(GameObject target)
    {
        yield return new WaitForSeconds(1f);
        traverse.Remove(target);
        partner.traverse.Remove(target);
    }
}
