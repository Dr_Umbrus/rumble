﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthQuake : MonoBehaviour
{
    GameManager gm;
    public GameObject shadow;
    public float timer;
    public float maxTimer;
    public int variante;


    private void Awake()
    {
        timer = maxTimer;
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = 999;
            StartCoroutine(Quaking());
        }
    }

    IEnumerator Quaking()
    {
        int rocks = Random.Range(7,13);

        gm.Shake(5, 35, 1.05f);
        for (int i = 0; i < rocks; i++)
        {
            Vector3 pos = Vector3.zero;
            pos.z = -2;
            pos.x = Random.Range(-gm.width, gm.width +0.1f);
            pos.y = Random.Range(-gm.height, gm.height -0.5f);
            Instantiate(shadow,pos, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(0.1f,0.35f));
        }

        timer = maxTimer + Random.Range(-variante, variante + 1) / 10;
    }
}
