﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NenupharManager : MonoBehaviour
{
    public NenupharMove[] nenuphars;
    public float timer, maxTimer;

    private void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            timer = maxTimer;
            int rand = Random.Range(0, nenuphars.Length);
            nenuphars[rand].MoveIn();
        }
    }

    private void Awake()
    {
        nenuphars = FindObjectsOfType<NenupharMove>();
    }
}
