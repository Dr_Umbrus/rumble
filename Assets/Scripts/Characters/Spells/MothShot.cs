﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MothShot : MonoBehaviour
{
    public float speed=0;
    public GameObject ball = null;
    public int id = -1;
    public int damage = 2;
    public Transform par = null;


    public void Shot(float angle)
    {
        GameObject temp=Instantiate(ball, transform.position, Quaternion.identity);
        temp.transform.rotation = Quaternion.Euler(0,0,angle - 90);
        temp.GetComponent<Projectile>().Create(id, false);
    }

    private void Update()
    {
        par.transform.Rotate(new Vector3(0, 0, speed * Time.deltaTime));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "player")
        {
            Character c = collision.GetComponent<Character>();
            if (c.playerNum != id)
            {
                c.Damage(damage, id, false, false);
            }
        }
        else if (collision.tag == "damageable")
        {
            Damageable d = collision.GetComponent<Damageable>();
            if (d.id != id)
            {
                d.Damage(damage, id, Vector2.zero, false, false);
            }
        }
    }
}
