﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class SmashBall : Damageable
{
    public bool bouncy = false;
    public int speed;
    public Vector2 direction;
    public GameObject treasure, bigTreasure, endPart;
    public int damageReceived = 0;
    public int threshold, thresholdValue;
    public StatsManager sm;
    public ParticleSystem ps;

    
    public Gradient grad;

    public float colorTime = 0;
     
    public Light2D lumiere;

    public override void Awakening()
    {
        ps = GetComponent<ParticleSystem>();
        direction = Random.insideUnitCircle;
        direction.Normalize();
        Destroy(gameObject, 20);
        lumiere = GetComponentInChildren<Light2D>();
        threshold = life - thresholdValue + Random.Range(-5, 6);
    }

    private void Update()
    {
        if (!bouncy)
        {
            transform.Translate(direction * speed * Time.deltaTime);
        }

        if (bouncy)
        {

            if (rb.velocity.magnitude < 0.2f)
            {
                
                rb.velocity = Vector2.zero;
                bouncy = false;
                direction = Random.insideUnitCircle;
                direction.Normalize();
            }
        }

        if (colorTime < 1)
        {
            colorTime += Time.deltaTime / 3;
        }
        else
        {
            colorTime = 0;
        }
        lumiere.color = grad.Evaluate(colorTime);
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!bouncy)
        {
            direction = Random.insideUnitCircle;
            direction.Normalize();
        }
    }


    public override void Dam(int source)
    {
        if (!dying)
        {
            if (life<=threshold)
            {
                threshold = life - thresholdValue + Random.Range(-5, 6);
                Vector2 pos = new Vector2(transform.position.x, transform.position.y);
                for (int i = 0; i < 10; i++)
                {
                    Instantiate(treasure, pos + Random.insideUnitCircle * 2, Quaternion.identity);
                    treasure.transform.Translate(new Vector3(0, 0, -0.15f));
                }

            }

            bouncy = true;
            aud.Play();
        }

        else
        {
            for (int i = 0; i < 3; i++)
            {
                Vector2 pos = new Vector2(transform.position.x, transform.position.y);
                GameObject temp = Instantiate(bigTreasure, pos + Random.insideUnitCircle * 2, Quaternion.identity);
                temp.GetComponent<BigTreasure>().SetTarget(source);
                bigTreasure.transform.Translate(new Vector3(0, 0, -0.15f));

            }
            Destroy(Instantiate(endPart, transform.position, Quaternion.identity), 2);
            sm.EndSmash();
        }
    }
}

