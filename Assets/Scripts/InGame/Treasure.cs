﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{
    public int value;
    bool ramasse=false;
    public AudioSource sounding;
    public GameObject paillettes;


    private void Awake()
    {
        transform.Translate(new Vector3(0, 0, -0.5f));

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!ramasse)
        {
            if (collision.gameObject.tag == "player")
            {
                collision.GetComponent<Character>().AddLoot(value);
                ramasse = true;
                StartCoroutine(End());
            }
        }
        
    }

    IEnumerator End()
    {
        GetComponent<SpriteRenderer>().color = Color.clear;
        Instantiate(paillettes, transform.position, Quaternion.identity);
        sounding.Play();
        while (sounding.isPlaying)
        {
            yield return 0;
        }
        Destroy(GetComponentInParent<Transform>().gameObject);
    }

}
