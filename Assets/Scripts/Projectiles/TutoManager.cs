﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class TutoManager : MonoBehaviour
{
    GameManager gm = null;
    [SerializeField]
    MoveIn info = null;

    [SerializeField]
    GameObject flecheH = null, flecheB = null;

    [SerializeField]
    GameObject[] realinfo= new GameObject[0];

    Vector2 potato = Vector2.zero;
    public bool move=false;

    int currentInfo=0;
    int oldInfo=0;
    bool watching = false;


    [SerializeField]
    GameObject cursor = null;

    [SerializeField]
    Text[] texts= new Text[0];

    [SerializeField]
    string[] names= new string[0];

    [SerializeField]
    int maxPage=0;
    int currentPage = 0;

    [SerializeField]
    AudioSource movement = null, activate = null, deactivate = null;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        ChangePage(0);
        for (int i = 0; i < realinfo.Length; i++)
        {
            realinfo[i].SetActive(false);
        }
        realinfo[0].SetActive(true);
    }

    private void Update()
    {
        oldInfo = currentInfo;
        if (!move)
        {
            if (potato.y > 0.5f)
            {
                if (currentInfo > 0)
                {
                    currentInfo--;
                }
                else
                {
                    if (currentPage > 0)
                    {
                        ChangePage(currentPage - 1);
                    }
                    else
                    {
                        ChangePage(maxPage);
                    }
                }
                StartCoroutine(MoveAgain());
                move = true;
            }
            else if (potato.y < -0.5f)
            {
                if(currentInfo>=1 && currentPage == maxPage)
                {
                    if (currentPage < maxPage)
                    {
                        ChangePage(currentPage + 1);
                    }
                    else
                    {
                        ChangePage(0);
                    }
                }
                else if (currentInfo < 4)
                {
                    currentInfo++;
                }
                else
                {
                    if (currentPage < maxPage)
                    {
                        ChangePage(currentPage + 1);
                    }
                    else
                    {
                        ChangePage(0);
                    }
                }
                
                StartCoroutine(MoveAgain());
                move = true;
            }

            if (watching)
            {
                if (potato.x > 0.5f)
                {
                    info.SideMe();
                    StartCoroutine(MoveAgain());
                    move = true;
                }
                else if (potato.x < -0.5f)
                {
                    info.SideMeBack();
                    StartCoroutine(MoveAgain());
                    move = true;
                }
            }
        }

        if (oldInfo != currentInfo)
        {
            for (int i = 0; i < realinfo.Length; i++)
            {
                realinfo[i].SetActive(false);
            }
            realinfo[currentInfo+5*currentPage].SetActive(true);
        }
        cursor.transform.position = texts[currentInfo].transform.position;

        if (currentPage == maxPage)
        {
            flecheB.SetActive(false);
        }
        else
        {
            flecheB.SetActive(true);
        }

        if (currentPage == 0)
        {
            flecheH.SetActive(false);
        }
        else
        {
            flecheH.SetActive(true);
        }
    }

    public void OnMove(InputValue value)
    {
        potato = value.Get<Vector2>();

    }

    public void ChangePage(int newPage)
    {
        if (newPage == maxPage && currentPage == 0)
        {
            currentInfo = 1;
        }
        else if (newPage >= currentPage)
        {
            currentInfo = 0;
        }
        else
        {
            if (newPage == 0 && currentPage==maxPage)
            {
                currentInfo = 0;
            }
            else
            {
                currentInfo = 4;
            }
            
        }
        currentPage = newPage;
        for (int i = 0; i < texts.Length; i++)
        {
            if (5 * currentPage + i < names.Length)
            {
                texts[i].text = names[5 * currentPage + i];
            }
            else
            {
                texts[i].text = "";
            }
        }

    }

    public void OnGrab()
    {
        if (!watching)
        {
            activate.Play();
            watching = true;
            info.MoveMe();
        }
    }

    public void OnBack()
    {
        if (!watching)
        {
            if (!gm.backSelection.isPlaying)
            {
                gm.backSelection.Play();
            }
            
            gm.BackMenu();
        }
        else
        {
            deactivate.Play();
            watching = false;
            info.MoveMe();
        }
    }

    IEnumerator MoveAgain()
    {
        movement.Play();
        yield return new WaitForSeconds(0.3f);
        move = false;
    }

}
